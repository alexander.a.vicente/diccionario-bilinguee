/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Operaciones;

/**
 *
 * @author Alex
 */
public class OperacionesControlador extends Operaciones {

    public double Sumar(double n1, double n2) {
        return n1 + n2;

    }

    public double Restar(double n1, double n2) {
        return n1 - n2;

    }

    public double Multiplicar(double n1, double n2) {
        return n1 * n2;

    }

    public double Dividir(double n1, double n2) {
        return (n1 / n2);

    }
}
